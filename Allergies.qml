import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Allergies"
    contentWidth: allergyCol.implicitWidth
    contentHeight: allergyCol.implicitHeight
    ColumnLayout {
        id: allergyCol
        x: allergyCol.implicitWidth > rootWin.width ?
               0 : (rootWin.width - allergyCol.implicitWidth) /2
        Label {
            id: t1
            text: "Do you know if you have any Allergies?"
            font.pointSize: 24
            opacity: 0
        }
        ColumnLayout {
            id: yesNowCol
            opacity: 0
            Layout.alignment: Qt.AlignHCenter
            spacing: b1.height

            Button {
                id: b1
                text: "Yes, I have one or more allergies"
                Material.accent: Material.Red
                highlighted: true
                Layout.alignment: Qt.AlignHCenter
                onClicked: {
                    showTheAllergies.start();
                }
            }
            Button {
                id: b2
                text: "No, I have no allergies that I know of"
                Material.accent: Material.Blue
                Layout.alignment: Qt.AlignHCenter
                highlighted: true
                onClicked: {
                    stackView.push("EmergencyContactInfo.qml");
                }
            }
        }

        GridLayout {
            id: allergyGrid
            columns: 3
            opacity: 0

            Label {
                font.pointSize: 16
                text: "Allergy"
            }
            Label {
                font.pointSize: 16
                text: "Reaction"
            }

            Label { //so we don't offset the delete button
                font.pointSize: 16
                text: ""
            }

            Component {
                id: allergyNameComp //0
                MedicationBox {
                    Layout.minimumWidth: addButton.width
                }

            }

            Component {
                id:reactionComp //1
                ComboBox {
                    id: reactionCombo
                    model: ["Mild (Rash, itching, stomach ache)",
                        "Moderate (scratchy throat, watery / itchy eyes)",
                        "Severe (Anaphylaxis, difficulty"+
                        " swallowing or breathing)",
                        "I don't know, somebody told me I was allergic to it"]
                    Layout.minimumWidth: textSizer.width +
                                         implicitIndicatorWidth
                    TextMetrics{
                        id: textSizer
                        text:  "Severe (Anaphylaxis, difficulty swallowing or"+
                               " breathing)"
                        font: reactionCombo.font

                    }
                }
            }


            Component {
                id: deleteButton //2
                Button {
                    //icon.name: "list-remove"
                    text: "Remove"
                    onClicked: {
                        text = "Kill me now!";
                        allergyGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }

            Component.onCompleted:  {

            }

            function makeNewRow() {
                allergyNameComp.createObject(allergyGrid,{});
                reactionComp.createObject(allergyGrid,{});
                deleteButton.createObject(allergyGrid,{});
            }

            function killTheZombie() {
                for(var i=3;i<allergyGrid.children.length;i+=3) {
                    var killBut = allergyGrid.children[i+2];
                    if(killBut.text.length > 6) {
                        var kill1 = allergyGrid.children[i];
                        var kill2 = allergyGrid.children[i+1];
                        var kill3 = allergyGrid.children[i+2];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }

        Button {
            id: addButton
            //icon.name: "list-add"
            text: "Add another allergy"
            opacity: 0
            onClicked: allergyGrid.makeNewRow();
            Material.accent: Material.Green
            highlighted: true

        }

        Button {
            id: doneButton
            text: "Done"
            Layout.alignment: Qt.AlignHCenter
            opacity: 0
            onClicked: {
                var alleryList = [];
                for(var i=3;i<allergyGrid.children.length;i+=3) {
                    var allergy = ({});
                    var allergyName = allergyGrid.children[i];
                    var reaction = allergyGrid.children[i+1];

                    allergy["allergyName"] = allergyName.text;
                    allergy["reaction"] = reaction.currentText;
                    alleryList.push(allergy);
                }
                saveProps["allergyList"] = alleryList;
                stackView.push("EmergencyContactInfo.qml");
            }
        }


    }

    SequentialAnimation {
        id: showTheAllergies
        PropertyAnimation {
            target: t1
            from: 1
            to: 0
            property: "opacity"
            duration: 300
        }

        PropertyAnimation {
            target: yesNowCol
            from: 1
            to: 0
            property: "opacity"
            duration: 300
        }
        ScriptAction {
            script:  {
                yesNowCol.visible = false;
                allergyGrid.makeNewRow();
            }
        }
        PropertyAnimation {
            target: allergyGrid
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PropertyAnimation {
            target: addButton
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }

        PropertyAnimation {
            target: doneButton
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: yesNowCol
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
