import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Basic Personal Information"
    contentWidth: basicPersonalColumn.implicitWidth
    contentHeight: basicPersonalColumn.implicitHeight
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: basicPersonalColumn
        x: basicPersonalColumn.implicitWidth >
                            rootWin.width ?
                                0 :
                                (rootWin.width -
                                 basicPersonalColumn.implicitWidth) /2

        Label {
            id: t1
            text: "Please tell me more about yourself:"
            font.pointSize: 24
            opacity: 0
        }
        ColumnLayout {
            id: theRest
            opacity: 0
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "First name"
                font.bold: true
            }
            TextField {
                id: firstName
                placeholderTextColor: "red"
                placeholderText: "Required"
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Middle name"
                font.bold: true
            }
            TextField {
                id: middleName
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Last name"
                font.bold: true
            }
            TextField {
                id: lastName
                placeholderTextColor: "red"
                placeholderText: "Required"
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "How would you like to be called"
                font.bold: true
            }
            TextField {
                id: preferredName
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Date of Birth"
                font.bold: true
            }
            DatePicker {
                id: dob
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Sex"
                font.bold: true
            }
            RowLayout {
                RadioButton {
                    id: male
                    text: "Male"
                }
                RadioButton {
                    id: female
                    text: "Female"
                }
                Label {
                    text: "Required"
                    color: "red"

                }

                ButtonGroup {
                    id: sexGroup
                    buttons: [male,female]
                }
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Gender (if different)"
                font.bold: true
            }
            TextField {
                id: gender
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Race"
                font.bold: true
            }
            ComboBox {
                id: race
                model: ["I choose not to answer",
                    "American Indian or Alaska native","Asian",
                    "Black or African American", "Polynesian","White","Other"]
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "Ethnicity"
                font.bold: true
            }
            ComboBox {
                id: ethnicity
                model: ["I choose not to answer","Hispanic Or Latino",
                    "Not Hispanic or Latino"]
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true}
            Label {
                text: "How did you hear about us?"
                font.bold: true
            }
            RowLayout {
                ComboBox {
                    id: refSource
                    model: ["Google", "I saw the sign", "Yelp", "From a friend"]
                    Layout.minimumWidth: 200
                }
                Label {
                    opacity: (refSource.currentIndex ==3) ? 1:0
                    text: "May we ask who?"
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 500
                        }
                    }
                }

                TextField {
                    opacity: (refSource.currentIndex ==3) ? 1:0
                    id: refName
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 500
                        }
                    }
                }
            }
            MenuSeparator {Layout.fillWidth: true}

            Button {
                text: "Next"
                opacity: (firstName.text.length > 0) &&
                         (lastName.text.length > 0) &&
                         (sexGroup.checkedButton != null) ? 1:0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 500
                    }
                }

                onClicked: {
                    saveProps["firstName"] = firstName.text;
                    saveProps["middleName"] = middleName.text;
                    saveProps["lastName"] = lastName.text;
                    saveProps["preferredName"] = preferredName.text;
                    saveProps["DOB"] = dob.getDate();
                    saveProps["frontEndDOB"] = dob.getFrontEndDate();
                    saveProps["sex"] = sexGroup.checkedButton.text;
                    saveProps["gender"] = gender.text;
                    saveProps["race"] = race.currentText;
                    saveProps["ethnicity"] = ethnicity.currentText;
                    saveProps["refSource"] = refSource.currentText;
                    if(refSource.currentIndex ==3) {
                        saveProps["refName"] = refName.text;
                    }

                    stackView.push("HomeAddrInfo.qml")
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 1000
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
