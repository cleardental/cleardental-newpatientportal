import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Page {
    property var systemName
    property var conditionList

    signal weAreDoneHere

    title: "Cancer"

    Label {
        id: name
        text: "Do you have any history of cancer?";
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        font.pointSize: 24
        wrapMode: Text.WordWrap
    }

    ColumnLayout {
        id: yesNoRow
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes, I have some history with cancer"
            Material.accent: Material.Red
            highlighted: true
            Layout.alignment: Qt.AlignHCenter
            onClicked:{
                yesNoRow.opacity=0;
                name.opacity=0;
                moreCondColumn.visible = true;
                moreCondColumn.opacity=1;
            }

        }
        Button {
            id: noButton
            text: "No, I have no history of cancer"
            Material.accent: Material.Blue
            highlighted: true
            onClicked:  {
                rootWin.saveProps["hxOfCancer"] = false;
                weAreDoneHere();
            }
            Layout.alignment: Qt.AlignHCenter
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    GridLayout {
        id: moreCondColumn
        visible: false
        opacity: 0
        columns: 2
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            text: "What type of cancer?"
            font.bold: true
        }

        TextField {
            id: cancerType
        }

        Label {
            text: "Did you have chemotherapy treatment?"
            font.bold: true
        }

        RowLayout {
            RadioButton {
                id: yesChemo
                text: "Yes"
                checked: false
            }
            RadioButton {
                id: noChemo
                text: "No"
                checked: true
            }
        }

        Label {
            text: "Did you have radiation treatment (head or neck)?"
            font.bold: true
        }

        RowLayout {
            RadioButton {
                id: yesRad
                text: "Yes"
                checked: false
            }
            RadioButton {
                id: noRad
                text: "No"
                checked: true
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }


    footer: Button {
        id: doneWithConButton
        text: "Done"
        opacity: moreCondColumn.visible ? 1:0
        Material.accent: Material.Green
        anchors.horizontalCenter: parent.horizontalCenter
        highlighted: true
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked: {
            rootWin.saveProps["hxOfCancer"] = true;
            rootWin.saveProps["cancerType"] = cancerType.text;
            rootWin.saveProps["hxChemo"] = yesChemo.checked;
            rootWin.saveProps["hxRad"] = yesRad.checked;
            weAreDoneHere();
        }
    }

}
