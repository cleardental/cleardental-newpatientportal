import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ScrollView {
    property string title: "Any Issues?"
    ColumnLayout {
        id: ccCol
        anchors.fill: parent
        anchors.leftMargin: ccCol.implicitWidth >
                            rootWin.width ?
                                0 :
                                (rootWin.width -
                                 ccCol.implicitWidth) /2
        Label {
            id: t1
            text: "Great! ☺️"
            font.pointSize: 24
            opacity: 0
        }
        ColumnLayout {
            id: theRest
            opacity: 0
            MenuSeparator {}
            Label {
                text: "Do you have any issues or concerns about anything" +
                      " in your mouth?"
                font.bold: true
            }
            RadioButton {
                id: ccNone
                text: "None at all"
            }
            RowLayout {
                RadioButton {
                    id: sen
                    text: "I have a sensitive tooth"
                }
                Label {
                    text: "Where:"
                    opacity: sen.checked ? 1:0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 500
                        }
                    }
                }
                TextField {
                    id: senWhere
                    placeholderText: "where the sensitivity is"
                    opacity: sen.checked ? 1:0
                    Layout.fillWidth: true
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 500
                        }
                    }
                }
            }
            RadioButton {
                id: looks
                text: "I don't like how my teeth look"
            }
            RadioButton {
                id: broken
                text: "I have a broken/chipped tooth I wish to have addressed"
            }
            RowLayout {
                RadioButton {
                    id: otherIssue
                    text: "I have a different issue"

                }
                Label {
                    text: "And what would that be?:"
                    opacity: otherIssue.checked ? 1:0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 500
                        }
                    }
                }
                TextField {
                    id: otherText
                    placeholderText: "Your issue"
                    opacity: otherIssue.checked ? 1:0
                    Behavior on opacity {
                        PropertyAnimation {
                            duration: 500
                        }
                    }
                }
            }
            ButtonGroup {
                id: ccGroup
                buttons: [ccNone,sen,looks,broken,otherIssue]
            }
            MenuSeparator{}
            Button {
                text: "Next"
                opacity: ccGroup.checkedButton != null  ? 1 :0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 1000
                    }
                }
                onClicked: {
                    if(sen.checked) {
                        saveProps["cc"] = "Sensitivity: " + senWhere.text
                    }
                    else if(otherIssue.checked) {
                        saveProps["cc"] = "Other: " + otherText.text
                    }
                    else {
                        saveProps["cc"] = ccGroup.checkedButton.text
                    }
                    stackView.push("BasicPersonalInfo.qml")
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 1000
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
