import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.12

Flickable {
    property string title: "Consent for treatment"
    contentWidth: consentColumn.implicitWidth
    contentHeight: consentColumn.implicitHeight
    interactive: false
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: consentColumn
        x: consentColumn.implicitWidth > rootWin.width ?
               0 : (rootWin.width - consentColumn.implicitWidth) /2
        Label {
            id: t1
            text: "Consent for treatment"
            font.pointSize: 24
            opacity: 0
            Layout.alignment: Qt.AlignHCenter
        }
        ColumnLayout {
            id: theRest
            opacity: 0
            Layout.alignment: Qt.AlignHCenter
            Label {
                text: "By signing here, you are agreeing to allowing the" +
                      " clinic to share your personal information with your"+
                      " dental plan and referred doctors."
                wrapMode: Text.Wrap
                Layout.maximumWidth: drawCanvas.width
                Layout.alignment: Qt.AlignHCenter
            }
            Canvas {
                id: drawCanvas
                Layout.preferredWidth: rootWin.width * .8
                Layout.preferredHeight: rootWin.height - 300
                Layout.alignment: Qt.AlignHCenter
                property int dotlength: 0
                property var dots: []
                onPaint: {
                    var ctx = getContext("2d");
                    ctx.clearRect(0, 0, width, height);
                    ctx.fillStyle = Qt.rgba(0, 0, 0, 1);
                    ctx.beginPath();
                    ctx.rect(1,1,width-1,height-1);
                    ctx.stroke();

                    ctx.beginPath();
                    for(var i=0;i<dots.length;i+=2) {
                        if(dots[i]===",") {
                            ctx.stroke();
                            ctx.beginPath();
                        }
                        else {
                            ctx.lineTo(dots[i],dots[i+1]);
                        }
                        //ctx.lineTo(dots[i],dots[i+1]);
                    }
                    ctx.stroke();
                }

                MouseArea {
                    anchors.fill: parent
                    onPositionChanged: {
                        drawCanvas.dots.push(mouseX);
                        drawCanvas.dots.push(mouseY);
                        drawCanvas.requestPaint();
                        drawCanvas.dotlength++;
                    }
                    onReleased: {
                        drawCanvas.dots.push(",");
                        drawCanvas.dots.push(",");
                    }
                }
            }
            Button {
                id: clearButton
                text: "Clear"
                Layout.alignment: Qt.AlignHCenter
                Material.accent: Material.Red
                highlighted: true
                onClicked: {
                    drawCanvas.dots = []
                    drawCanvas.dotlength =0;
                    drawCanvas.requestPaint();
                }
            }
            Button {
                id: doneButton
                text: "Done"
                Layout.alignment: Qt.AlignHCenter
                opacity: (drawCanvas.dotlength>5) ? 1 : 0
                Material.accent: Material.Green
                highlighted: true
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 500
                    }
                }
                onClicked: {
                    saveProps["consentSigDots"] = drawCanvas.dots
                    saveProps["consentSigDotCount"] = drawCanvas.dotlength
                    stackView.push("FinishPage.qml");
                }
            }
        }
    }



    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 1000
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
