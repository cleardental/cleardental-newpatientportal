import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ScrollView {
    property string title: "Dental history"
    ColumnLayout {
        id: denInfoCol
        anchors.fill: parent
        anchors.leftMargin: denInfoCol.implicitWidth >
                            rootWin.width ?
                                0 :
                                (rootWin.width -
                                 denInfoCol.implicitWidth) /2
        Label {
            id: t1
            text: "Now lets talk a little about your dental history"
            font.pointSize: 24
            opacity: 0
            wrapMode: Text.WordWrap
            Layout.maximumWidth: rootWin.width
        }
        ColumnLayout {
            id: theRest
            opacity: 0
            width: parent.width
            MenuSeparator{}
            Label {
                text: "Do you have any past issues with the dentist?"
                font.bold: true
            }
            ComboBox {
                id: pastIssues
                model: ["No issues at all",
                    "No issues, but I don't like seeting the dentist though",
                    "I have had an issue in the dental chair",
                    "I am deathly scared of the dentist"]
                Layout.fillWidth: true
                Layout.minimumWidth: 375
            }
            MenuSeparator{}
            Label {
                text: "When was your last cleaning?"
                font.bold: true
            }
            ComboBox {
                id: lastCleaning
                model: ["About 6 months to 12 months ago",
                    "Less than 6 months ago",
                    "More than a year ago",
                    "More than 2 years ago",
                    "More than 10 years ago / I can't remember"]
                Layout.fillWidth: true
            }
            MenuSeparator{}
            Label {
                text: "Have you ever had \"scaling and root planing\" or\n"+
                      " a \"deep cleaning\" done before?"
                font.bold: true
            }
            ComboBox {
                id: lastSCRP
                model: ["Yes; I had it done less than 2 years ago",
                    "Yes; but I had it done more than 2 years ago",
                    "No, I have never had it done before",
                    "I don't even know what you are talking about"]
                Layout.fillWidth: true
            }
            MenuSeparator {}
            Label {
                text: "How often do you brush?"
                font.bold: true
            }
            ComboBox {
                id: oftenBrush
                model: ["Twice a day",
                    "Daily",
                    "Weekly",
                    "Never"]
                Layout.fillWidth: true
            }

            MenuSeparator {}
            Label {
                text: "How often do you floss?"
                font.bold: true
            }
            ComboBox {
                id: oftenFloss
                model: ["Twice a day",
                    "Daily",
                    "Weekly",
                    "Never"]
                Layout.fillWidth: true
            }

            MenuSeparator {}
            Label {
                text: "Does your gum bleed when you brush or floss?"
                font.bold: true
            }
            ComboBox {
                id: oftenBleed
                model: ["Never",
                    "Sometimes",
                    "Rarely",
                    "Every time"]
                Layout.fillWidth: true
            }

            MenuSeparator {}
            Label {
                text: "Do you have headaches?"
                font.bold: true
            }
            ComboBox {
                id: oftenHeadaches
                model: ["Never",
                    "Sometimes",
                    "All the time"]
                Layout.fillWidth: true
            }

            MenuSeparator {}
            Label {
                text: "Do you know if you grind at night?"
                font.bold: true
            }
            ComboBox {
                id: oftenGrinding
                model: ["Never",
                    "No clue",
                    "Sometimes",
                    "Every night"]
                Layout.fillWidth: true
            }

            MenuSeparator {}
            Label {
                text: "What are you looking to get done?"
                font.bold: true
            }
            ComboBox {
                id: lookingToGetDone
                model: ["Just a Cleaning and Exam",
                    "I think I might need a filling",
                    "I want my teeth to look better",
                    "I don't know, you tell me"]
                Layout.fillWidth: true
            }

            MenuSeparator {}
            Label {
                text: "Have you ever had braces before?"
                font.bold: true
            }
            ComboBox {
                id: bracesBefore
                model: ["No, never",
                    "Yes, back when I was a kid",
                    "Yes, I had it done just a few years ago"]
                Layout.fillWidth: true
            }
            MenuSeparator {}


            Label {
                text: "Do you have any trouble sleeping?"
                font.bold: true
            }
            ComboBox {
                id: anyTroubleSleeping
                model: ["No, I have no issues with sleeping",
                    "Yes, I sometimes wake up in the middle of the night",
                    "I have been diagnosed with Sleep apnea"]
                Layout.fillWidth: true
            }

            MenuSeparator {}


            Button {
                text: "Next"
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 1000
                    }
                }
                onClicked: {
                    saveProps["pastIssues"] = pastIssues.currentText;
                    saveProps["lastCleaning"] = lastCleaning.currentText;
                    saveProps["lastSCRP"] = lastSCRP.currentText;
                    saveProps["oftenBrush"] = oftenBrush.currentText;
                    saveProps["oftenFloss"] = oftenFloss.currentText;
                    saveProps["oftenBleed"] = oftenBleed.currentText;
                    saveProps["oftenHeadaches"] = oftenHeadaches.currentText;
                    saveProps["oftenGrinding"] = oftenGrinding.currentText;
                    saveProps["lookingToGetDone"] =
                            lookingToGetDone.currentText;
                    saveProps["bracesBefore"] = bracesBefore.currentText;

                    stackView.push("MedicalHistory.qml");
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
