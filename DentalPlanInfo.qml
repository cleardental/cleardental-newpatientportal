import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Dental Plan"
    contentWidth: dplanInfoColumn.implicitWidth
    contentHeight: dplanInfoColumn.implicitHeight
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: dplanInfoColumn
        x: dplanInfoColumn.implicitWidth > rootWin.width ?
               0 : (rootWin.width - dplanInfoColumn.implicitWidth) /2
        Label {
            id: t1
            text: "Do you have a dental plan of some kind?"
            font.pointSize: 24
            opacity: 0
        }

        Button {
            id: b1
            text: "Yes, I do have a dental plan"
            opacity: 0
            onClicked: {
                firstIns.opacity = 1;
            }
        }
        Button {
            id: b2
            text: "No, I don't have a\ndental plan of any kind"
            opacity: 0
            onClicked: {
                saveProps["hasDentalPlan"] = false;
                if(saveProps["pain"]) {
                    stackView.push("MedicalHistory.qml");
                }
                else {
                    stackView.push("DentalHistory.qml");
                }
            }
        }

        GridLayout {
            id: firstIns
            opacity: 0
            columns: 2

            Label {
                text: "Insured’s Name"
                font.bold: true
            }
            TextField {
                id: insName
                Layout.fillWidth: true
            }
            Label {
                text: "Relationship to Patient"
                font.bold: true
            }
            ComboBox {
                id: insRelToPat
                model: ["Self", "Parent", "Spouse"]
                editable: true
                Layout.fillWidth: true
            }
            Label {
                text: "Social Security #"
                font.bold: true
            }
            TextField {
                id: insSSN
                Layout.fillWidth: true
            }
            Label {
                text: "Insurance Company"
                font.bold: true
            }
            TextField {
                id: insCompany
                Layout.fillWidth: true
            }
            Label {
                text: "Group Number"
                font.bold: true
            }
            TextField {
                id: insGroup
                Layout.fillWidth: true
            }
            Label {
                text: "Subscriber ID"
                font.bold: true
            }
            TextField {
                id: insSubID
                Layout.fillWidth: true
            }
            Label {
                text: "Insurance Company Address"
                font.bold: true
            }
            TextField {
                id: insAddr
                Layout.fillWidth: true
            }
            Label {
                text: "Do you have additional coverage or\na secondary"+
                      " insurance?"
                font.bold: true
            }
            Row {
                RadioButton {
                    id: yesAddr
                    text: "Yes"
                }
                RadioButton {
                    id: noAddr
                    text: "No"
                    checked: true
                }
            }
            MenuSeparator {Layout.columnSpan:2}
            Button {
                text: "Next"
                onClicked: {
                    saveProps["hasDentalPlan"] = true;
                    saveProps["insName"] = insName.text;
                    saveProps["insRelToPat"] = insRelToPat.currentText;
                    saveProps["insSSN"] = insSSN.text;
                    saveProps["insCompany"] = insCompany.text;
                    saveProps["insGroup"] = insGroup.text;
                    saveProps["insSubID"] = insSubID.text;
                    saveProps["insAddr"] = insAddr.text;
                    if(yesAddr.checked) {
                        stackView.push("SecondaryDentalPlanInfo.qml")
                    } else {
                        if(saveProps["pain"]) {
                            stackView.push("MedicalHistory.qml");
                        }
                        else {
                            stackView.push("DentalHistory.qml");
                        }
                    }
                }
            }
            Behavior on opacity {
                PropertyAnimation {
                    duration: 1000
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: b1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PropertyAnimation {
            target: b2
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
