import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ScrollView {
    property string title: "Additional Contact informtion"
    ColumnLayout {
        id: addContactInfo
        anchors.fill: parent
        anchors.leftMargin: addContactInfo.implicitWidth >
                            rootWin.width ?
                                0 :
                                (rootWin.width -
                                 addContactInfo.implicitWidth) /2
        Label {
            id: t1
            text: "In order to send you appointment reminders, we will need" +
                  " additional contact information."
            font.pointSize: 24
            opacity: 0
            Layout.maximumWidth: rootWin.width
            wrapMode: Text.WordWrap
        }

        ColumnLayout {
            id: theRest
            opacity: 0
            //Layout.maximumWidth: 500
            MenuSeparator {Layout.fillWidth: true;}
            Label {
                text: "Home phone number"
                font.bold: true
            }
            TextField {
                id: homePhoneNumber
                inputMethodHints: Qt.ImhDialableCharactersOnly
                Layout.fillWidth: true
            }
            Label {
                text: "Cell number"
                font.bold: true
            }
            TextField {
                id: cellNumber
                Layout.fillWidth: true
                inputMethodHints: Qt.ImhDialableCharactersOnly
            }
            Label {
                text: "Work number"
                font.bold: true
            }
            TextField {
                id: workNumber
                Layout.fillWidth: true
                inputMethodHints: Qt.ImhDialableCharactersOnly
            }
            Label {
                text: "Email address"
                font.bold: true
            }
            TextField {
                id: emailAddr
                Layout.fillWidth: true
            }
            Label {                
                text: "WhatsApp number"
                font.bold: true
            }
            TextField {
                id: whatsAppNumb
                Layout.fillWidth: true
            }
            Label {
                text: "Facebook Name or URL"
                font.bold: true
            }
            TextField {
                id: facebookName
                Layout.fillWidth: true
            }
            Label {
                text: "Instagram name"
                font.bold: true
            }
            TextField {
                id: instaName
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true;}
            Label {
                text: "Best contact method"
                font.bold: true
            }
            ComboBox {
                id: bestContactBox
                model: ["Call cell", "Text me", "Call home", "Email Me",
                    "Call work", "WhatsApp", "Facebook Message", "Instagram"]
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.fillWidth: true;}
            Label {
                text: "Days you prefer to come in"
                font.bold: true
            }
            RowLayout {
                id: dayRow
                Button {
                    id: monCheck
                    text: "Mon"
                    checkable: true
                }
                Button {
                    id: tueCheck
                    text: "Tue"
                    checkable: true
                }
                Button {
                    id: wedCheck
                    text: "Wed"
                    checkable: true
                }
                Button {
                    id: thurCheck
                    text: "Thur"
                    checkable: true
                }
                Button {
                    id: friCheck
                    text: "Fri"
                    checkable: true
                }
                Button {
                    id: satCheck
                    text: "Sat"
                    checkable: true
                }
            }
            MenuSeparator {Layout.fillWidth: true;}

            Button {
                text: "Next"
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 1000
                    }
                }
                onClicked: {
                    saveProps["homePhoneNumber"] = homePhoneNumber.text;
                    saveProps["cellNumber"] = cellNumber.text;
                    saveProps["workNumber"] = workNumber.text;
                    saveProps["emailAddr"] = emailAddr.text;
                    saveProps["whatsAppNumb"] = whatsAppNumb.text;
                    saveProps["facebookName"] = facebookName.text;
                    saveProps["instaName"] = instaName.text;
                    saveProps["bestContact"] = bestContactBox.currentText;
                    var availableStr = "";
                    if(monCheck.checked) {
                        availableStr += "Mon,";
                    }
                    if(tueCheck.checked) {
                        availableStr += "Tue,";
                    }
                    if(wedCheck.checked) {
                        availableStr += "Wed,";
                    }
                    if(thurCheck.checked) {
                        availableStr += "Thur,";
                    }
                    if(friCheck.checked) {
                        availableStr += "Fri,";
                    }
                    if(satCheck.checked) {
                        availableStr += "Sat,";
                    }
                    if(availableStr.length > 0) {
                        availableStr = availableStr.slice(0, -1);
                        saveProps["available"] = availableStr;
                    }

                    stackView.push("DentalPlanInfo.qml");
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
