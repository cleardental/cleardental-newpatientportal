import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Emergency Contact informtion"
    contentWidth: emerContInfoCol.implicitWidth
    contentHeight: emerContInfoCol.implicitHeight
    ColumnLayout {
        id: emerContInfoCol
        x: emerContInfoCol.implicitWidth > rootWin.width ?
               0 : (rootWin.width - emerContInfoCol.implicitWidth) /2
        Label {
            id: t1
            text: "Now we need to ask you about your emergency contact"
            font.pointSize: 24
            opacity: 0
            Layout.maximumWidth: rootWin.width
            wrapMode: Text.WordWrap
        }
        GridLayout {
            id: theRest
            opacity: 0
            columns: 2
            MenuSeparator {Layout.columnSpan: 2}
            Label {
                text: "Contact name"
                font.bold: true
            }
            TextField {
                id:emerName
                placeholderTextColor: "red"
                placeholderText: "Required"
                Layout.fillWidth: true

            }
            Label {
                text: "Relationship with contact"
                font.bold: true
            }
            TextField {
                id: emerRel
                placeholderTextColor: "red"
                placeholderText: "Required"
                Layout.fillWidth: true
            }
            Label {
                text: "Contact's phone number"
                font.bold: true
            }
            TextField {
                id: emerPhone
                placeholderTextColor: "red"
                placeholderText: "Required"
                Layout.fillWidth: true
            }

            Button {
                text: "Next"
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 1000
                    }
                }
                opacity: (emerName.text.length > 0) &&
                         (emerRel.text.length > 0) &&
                         (emerPhone.text.length > 0) ? 1:0
                onClicked: {
                    saveProps["emergencyName"] = emerName.text;
                    saveProps["emergencyRel"] = emerRel.text;
                    saveProps["emergencyPhone"] = emerPhone.text;
                    stackView.push("Consent.qml");
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
