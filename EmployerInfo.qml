import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

//TODO: Figure out of this is even needed...

ScrollView {
    property string title: "Employer Information"
    ColumnLayout {
        anchors.centerIn: parent
        Label {
            id: t1
            text: "Please fill out the following about your work place"
            font.pointSize: 24
            opacity: 0
        }
        GridLayout {
            id: theRest
            opacity: 0
            columns: 2
            Label {
                text: "Occupation"
            }
            TextField {
                Layout.fillWidth: true

            }
            Label {
                text: "Employer"
            }
            TextField {
                Layout.fillWidth: true

            }
            Label {
                text: "Business Phone"
            }
            TextField {
                Layout.fillWidth: true

            }

            Button {
                text: "Next"
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 1000
                    }
                }
                onClicked: {
                    if(ccNone.checked) {

                    }
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
