import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ScrollView {
    property string title: "Finished!"

    property bool doneSending: false

    function sendPayload() {
        var sendMe = JSON.stringify(saveProps, null, '\t');
        console.debug(sendMe);
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://clear.dental/submit/",false);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                doneSending = true;
                console.debug(xhr.responseText);
                finishUp();
            }
        }
        xhr.send(sendMe);
    }

    function finishUp() {
        allIsGoodAnimation.start();
    }

    function sendFakePayload() {
        var sendMe = '{
    "pain": false,
    "cc": "Sensitivity: everywhere",
    "firstName": "Jane",
    "middleName": "A",
    "lastName": "Doe",
    "preferredName": "Jainie",
    "DOB": "7/8/1991",
    "frontEndDOB": "Jul-8-1991",
    "sex": "Female",
    "gender": "",
    "race": "Other",
    "ethnicity": "Not Hispanic or Latino",
    "refSource": "From a friend",
    "refName": "Kathy",
    "homeAddr1": "23 Fantasy Ave",
    "homeAddr2": "Apt 123",
    "homeCity": "Jidor",
    "homeState": "ME",
    "homeZip": "654987",
    "homeCountry": "",
    "homePhoneNumber": "HomePhone",
    "cellNumber": "CellPhone",
    "workNumber": "WorkPhone",
    "emailAddr": "Email@example.com",
    "whatsAppNumb": "Whattsup!",
    "facebookName": "FacebookMe!",
    "instaName": "Intsta mee",
    "bestContact": "Instagram",
    "available": "Fri,Sat",
    "hasDentalPlan": true,
    "insName": "Me",
    "insRelToPat": "Self",
    "insSSN": "123-32-1234",
    "insCompany": "Company",
    "insGroup": "Groupa Numba",
    "insSubID": "Suba",
    "insAddr": "Insua",
    "pastIssues": "No issues, but I don\'t like seeting the dentist though",
    "lastCleaning": "About 6 months to 12 months ago",
    "lastSCRP": "Yes; I had it done less than 2 years ago",
    "oftenBrush": "Twice a day",
    "oftenFloss": "Twice a day",
    "oftenBleed": "Never",
    "oftenHeadaches": "Sometimes",
    "oftenGrinding": "Never",
    "lookingToGetDone": "Just a Cleaning and Exam",
    "bracesBefore": "Yes, back when I was a kid",
    "medicalConditions": {
        "Hyperthyroidism": "Way too much!",
        "pregnant": true,
        "pregnant-duedate": "9/14/2021",
        "pregnant-OBGYN": "Dr. Know"
    },
    "emergencyName": "sdf",
    "emergencyRel": "sdf",
    "emergencyPhone": "fadsasd",
    "consentSigDots": [
        95,
        78,
        112,
        92,
        140,
        112,
        174,
        136,
        242,
        186,
        300,
        228,
        346,
        260,
        364,
        276,
        380,
        292,
        410,
        326,
        414,
        330,
        416,
        332,
        ",",
        ",",
        413,
        82,
        408,
        82,
        399,
        86,
        389,
        93,
        374,
        101,
        355,
        111,
        304,
        132,
        245,
        163,
        197,
        195,
        165,
        215,
        139,
        229,
        117,
        243,
        109,
        247,
        101,
        255,
        93,
        259,
        80,
        269,
        77,
        272,
        75,
        274,
        ",",
        ","
    ],
    "consentSigDotCount": 30
}'
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://clear.dental/submit/",false);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                doneSending = true;
                console.debug(xhr.responseText);
                finishUp();
            }
        }
        xhr.send(sendMe);


    }

    ColumnLayout {
        id: finishCol
        anchors.fill: parent
        anchors.leftMargin: (rootWin.width - finishCol.implicitWidth) /2
        Label {
            id: t1
            text: "Sending your data, please wait..."
            Layout.maximumWidth: rootWin.width
            font.pointSize: 24
            wrapMode: Text.Wrap
            opacity: 0
        }
        BusyIndicator {
            id: b1
            opacity: 0
        }

        ColumnLayout {
            id: errorCol
            opacity: 0
            Label {
                text: "We could not connect to the server"
                font.pointSize: 24
                color: Material.color(Material.Red)
            }

            Label {
                text: "Please contact our office at XXX-XXX-XXXX"
            }
        }

        ColumnLayout {
            id: theRest
            opacity: 0
            Label {
                text: "Your information has been sent! ✓"
                font.pointSize: 24
                color: Material.color(Material.Green)
            }

            Label {
                text: "You may close this window"
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 200
        }
        PropertyAnimation {
            target: b1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    SequentialAnimation {
        id: allIsGoodAnimation
        ParallelAnimation {
            PropertyAnimation {
                target: t1
                from: 1
                to: 0
                property: "opacity"
                duration: 300
            }
            PropertyAnimation {
                target: b1
                from: 1
                to: 0
                property: "opacity"
                duration: 300
            }
            ScriptAction {
                script: b1.running = false;
            }
        }
        PropertyAnimation {
            target:theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Timer {
        id: errorTimer
        interval: 5000;
        onTriggered: {
            if(!doneSending) {
                theSystemIsDown.start();
                errorTimer.stop();
            }
            else {
                allIsGoodAnimation.start();
            }

        }
    }

    SequentialAnimation {
        id: theSystemIsDown //down down down down down down
        ParallelAnimation {
            PropertyAnimation {
                target: t1
                from: 1
                to: 0
                property: "opacity"
                duration: 300
            }
            PropertyAnimation {
                target: b1
                from: 1
                to: 0
                property: "opacity"
                duration: 300
            }
            ScriptAction {
                script: b1.running = false;
            }
        }
        PropertyAnimation {
            target:errorCol
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
        sendPayload();
    }
}
