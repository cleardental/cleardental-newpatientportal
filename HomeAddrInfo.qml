import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Home Address"
    contentWidth: personalAddrColumn.implicitWidth
    contentHeight: personalAddrColumn.implicitHeight
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: personalAddrColumn
        x: personalAddrColumn.implicitWidth >
                            rootWin.width ?
                                0 :
                                (rootWin.width -
                                 personalAddrColumn.implicitWidth) /2
        Label {
            id: t1
            text: "Please fill out your <b>home</b> address"
            font.pointSize: 24
            opacity: 0
        }
        GridLayout {
            id: theRest
            opacity: 0
            columns: 2
            MenuSeparator {Layout.columnSpan:2}
            Label {text: "Street Address (Line 1)";font.bold: true}
            TextField {
                id:addr1;
                placeholderText: "Required"
                placeholderTextColor: "red"
                Layout.fillWidth: true
            }

            Label {text: "Street Address (Line 2)";font.bold: true}
            TextField {
                id:addr2;
                Layout.fillWidth: true
            }

            Label {text: "City";font.bold: true}
            TextField{
                id:city;
                placeholderText: "Required"
                placeholderTextColor: "red"
                Layout.fillWidth: true
            }

            Label {text: "Your State";font.bold: true}
            ComboBox {
                id: state
                model: ["AK","AL","AR","AZ","CA","CO", "CT","DC","DE","FL","GA",
                    "GU","HI","IA","ID", "IL","IN","KS","KY","LA","MA","MD",
                    "ME","MH","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ",
                    "NM","NV","NY", "OH","OK","OR","PA","PR","PW","RI","SC",
                    "SD","TN","TX","UT","VA","VI","VT","WA","WI","WV","WY"]
                Component.onCompleted: state.currentIndex = state.find("MA")
                Layout.fillWidth: true
            }

            Label {text: "Zip code";font.bold: true}
            TextField{
                id: zip
                placeholderText: "Required"
                placeholderTextColor: "red"
                inputMethodHints: Qt.ImhDigitsOnly
                Layout.fillWidth: true
            }

            Label {text: "Country";font.bold: true}
            TextField{id:country
                placeholderText: "If not in USA, enter country"
                Layout.fillWidth: true
            }

            Button {
                text: "Next"
                onClicked: {
                    saveProps["homeAddr1"] = addr1.text;
                    saveProps["homeAddr2"] = addr2.text;
                    saveProps["homeCity"] = city.text;
                    saveProps["homeState"] = state.currentText;
                    saveProps["homeZip"] = zip.text;
                    saveProps["homeCountry"] = country.text;

                    if(saveProps["pain"]) {
                        stackView.push("QuickContactInfo.qml");
                    }
                    else {
                        stackView.push("DetailedContactInfo.qml");
                    }
                }

                opacity: addr1.text.length > 0 &&
                         city.text.length > 0 &&
                         zip.text.length > 0 ? 1 : 0

                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }

            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 1000
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
