import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Medical history"
    contentWidth: medColumn.implicitWidth
    contentHeight: medColumn.implicitHeight
    ScrollBar.vertical: ScrollBar { }

    function goBackCondition(){
        if(mainSwipeView.currentIndex == 0 ) {
            stackView.pop();
        }
        else {
            mainSwipeView.currentIndex--;
        }
    }

    ColumnLayout {
        id: medColumn
        x: medColumn.implicitWidth > rootWin.width ?
               0 : (rootWin.width - medColumn.implicitWidth) /2

        Label {
            id: t1
            text: "To prevent any complications\n" +
                  "please tell us about your\n" +
                  "medical history"
            font.pointSize: 24
            opacity: 0
            Layout.alignment: Qt.AlignHCenter
        }

        ListView {
            id: mainSwipeView

            Layout.maximumWidth: rootWin.width *.8
            Layout.minimumWidth: rootWin.width *.8

            Layout.minimumHeight: rootWin.height - 150
            Layout.maximumHeight:  rootWin.height - 150

            function saveConditions() {
                saveProps["medicalConditions"] = mainSwipeView.medicalConditionList;
            }

            orientation: ListView.Horizontal

            opacity: 0
            clip: true
            snapMode: ListView.SnapOneItem
            interactive: false

            highlightFollowsCurrentItem: true
            highlightMoveVelocity: -1
            highlightMoveDuration: 200
            preferredHighlightBegin: 0
            preferredHighlightEnd: 10


            Component.onCompleted:  {
                currentIndex= -1
                currentIndex= 0;
            }


            model: pageArray.length
            spacing: mainSwipeView.width /2
            property var medicalConditionList: ({})

            displaced: Transition {
                      NumberAnimation { properties: "x,y"; duration: 100 }
                  }

            delegate: Loader {
                sourceComponent: mainSwipeView.pageArray[index]
            }


            property var pageArray: [heartPage,metabolic,gi,bone,resp,
                orgDis,infectDis,neuro,immune,psychotic,cancerHx,premedNeeded,
                otherCond,preg
            ]

            Component {
                id: heartPage
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Heart/Blood disorders"
                    conditionList: ["Artificial Heart Valve(s)",
                        "Congenital Heart Defects",
                        "Heart Murmurs",
                        "Angina",
                        "Congestive Heart Failure",
                        "Heart Surgery",
                        "Pacemaker / defibrillator",
                        "Bacterial Endocarditis",
                        "Coronary Artery Disease",
                        "High Blood Pressure",
                        "Low Blood Pressure",
                        "Abnormal Bleeding",
                        "Methemoglobinemia",
                        "Hemophillia",
                        "Anemia",
                        "Other Heart Conditions"
                    ]
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: metabolic
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Metabolic / Endocrine disorders";
                    conditionList: ["Diabetes",
                        "Hypothyroidism",
                        "Hyperthyroidism",
                        "Other metabolic or endocrine disorder"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component  {
                id: gi
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Gastrointestinal disorders";
                    conditionList: ["G.E. Re-flux / Heartburn",
                        "Gastric Ulcers / Gastritis",
                        "Inflammatory Bowell Disease",
                        "Other Gastrointestinal disorders"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: bone
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Bone / Joint disorders";
                    conditionList: ["Artificial joint",
                        "Osteoporosis",
                        "History of Bisphosphonates"+
                        " (including Fosamax, Actonel, Boniva)",
                        "Other Bone / Joint disorders"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: resp
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Respiratory / Lung disorders";
                    conditionList: ["Asthma",
                        "Emphysema or COPD",
                        "Bronchitis",
                        "Other Respiratory / Lung condition"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: orgDis
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Organ disorders";
                    conditionList: ["Kidney Disease",
                        "Liver disease",
                        "Eye disease (including glaucoma)",
                        "Other organ disorder"];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: infectDis
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Infectious disease";
                    conditionList: ["AIDS / HIV",
                        "Hepatitis",
                        "Other Sexually Transmitted Disease",
                        "Tuberculosis",
                        "Other infectious disease"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: neuro
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Neurological disorders";
                    conditionList: ["Epilepsy",
                        "Stroke",
                        "Migrine",
                        "Other Neurological disorders"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: immune
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Immune diseases";
                    conditionList: ["Lupus",
                        "Rheumatoid Arthritis",
                        "Sjögren’s syndrome",
                        "Myasthenia Gravis",
                        "Other immune diseases"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: psychotic
                SystemReviewPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    systemName: "Psychotic / Behavioral disorders";
                    conditionList: ["ADD / ADHD",
                        "Anxiety / Panic Attacks",
                        "Eating disorder",
                        "Other behavioral disorders"
                    ];
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: cancerHx
                CancerHistoryPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: premedNeeded
                PremedPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    onWeAreDoneHere: mainSwipeView.currentIndex++
                }
            }

            Component {
                id: otherCond
                OtherConditionsPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    onWeAreDoneHere: {
                        if(saveProps["sex"]=== "Female") {
                            mainSwipeView.currentIndex++
                        }
                        else {
                            mainSwipeView.saveConditions();
                            stackView.push("Medications.qml");
                        }
                    }
                }
            }

            Component {
                id: preg
                PregnancyPage {
                    width: mainSwipeView.width - 20
                    height: mainSwipeView.height - 20
                    onWeAreDoneHere: {
                        mainSwipeView.saveConditions();
                        stackView.push("Medications.qml");
                    }
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: mainSwipeView
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
