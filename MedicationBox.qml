import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

TextField {
    id: medBoxRoot

    signal newDrugType(string drugType)

    property var drugName;

//    onTextEdited: {
//        if(text.length > 2) {
//            listPopup.updateList(text);
//            if(!listPopup.visible) {
//                listPopup.open();
//            }
//        } else {
//            listPopup.close();
//        }
//    }


    Popup {
        id: listPopup
        property var fullList: [];
        width: parent.width
        height: 300
        x: 0
        y:  parent.height

        ListModel {id:popModel}

        ListView {
            model: popModel
            width: parent.width
            height: parent.height
            delegate: Button {
                text: drugName
                width: parent.width
                flat: true
                onClicked: {
                    medBoxRoot.text = drugName;
                    //newDrugType(whatFor);
                    listPopup.close();
                }
            }
            clip: true
        }

        function updateList(currentInput) {
            popModel.clear();
//            console.debug(drugListModel.drugList.length);
//            for(var i=0;i<drugListModel.drugList.length;i++) {
//                if(drugListModel.drugList[i].toUpperCase().includes(currentInput.toUpperCase())) {
//                    popModel.append({"drugName": drugListModel.drugList[i]});
//                }
//            }
        }


    }

}
