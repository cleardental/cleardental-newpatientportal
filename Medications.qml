import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Medication List"
    contentWidth: medListColumn.implicitWidth
    contentHeight: medListColumn.implicitHeight
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: medListColumn
        x: medListColumn.implicitWidth > rootWin.width ?
               0 : (rootWin.width - medListColumn.implicitWidth) /2
        Label {
            id: t1
            text: "Are you taking any medications including herbal supplements?"
            font.pointSize: 24
            opacity: 0
            wrapMode: Text.WordWrap
            Layout.maximumWidth: rootWin.width
            Layout.alignment: Qt.AlignHCenter
        }
        ColumnLayout {
            id: yesNowCol
            opacity: 0
            spacing: b1.height
            Layout.alignment: Qt.AlignHCenter

            Button {
                id: b1
                text: "Yes, I am taking medications"
                Material.accent: Material.Red
                highlighted: true
                onClicked: {
                    showtheDrugs.start();
                }
                Layout.alignment: Qt.AlignHCenter
            }
            Button {
                id: b2
                text: "No, I am not taking any medication\nnor any supplements"
                Material.accent: Material.Blue
                highlighted: true
                onClicked: {
                    stackView.push("Allergies.qml");
                }
                Layout.alignment: Qt.AlignHCenter
            }
        }

        GridLayout {
            id: medGrid
            columns: 5
            opacity: 0


            Label {
                font.pointSize: 16
                text: "Medication Name"
            }
            Label {
                font.pointSize: 16
                text: "What for"
            }

            Label {
                font.pointSize: 16
                text: "Amount"
            }

            Label {
                font.pointSize: 16
                text: "How often"
            }

            Label { //so we don't offset the delete button
                font.pointSize: 16
                text: ""
            }

            Component { //0
                id: drugNameComp
                MedicationBox {
                    Layout.minimumWidth: 175
                    Layout.fillWidth: true
                }

            }

            Component { //1
                id: usedForText
                TextField {

                }
            }

            Component { //2
                id: amountField
                TextField {

                }
            }

            Component { //3
                id:scheduleField
                ComboBox {
                    property var setMe;
                    model: ["Once a day", "Twice a day", "3 times a day",
                        "4 times a day", "Once a week" , "As I need to"]
                    Layout.minimumWidth: implicitBackgroundWidth +
                                         implicitIndicatorWidth
                }
            }


            Component {
                id: deleteButton
                Button {
                    //icon.name: "list-remove"
                    text: "Remove"
                    onClicked: {
                        text = "Kill me now!";
                        medGrid.killTheZombie();
                    }
                    Material.accent: Material.Red
                    highlighted: true
                }
            }

            Component.onCompleted:  {

            }

            function makeNewRow() {
                drugNameComp.createObject(medGrid,{});
                usedForText.createObject(medGrid,{});
                amountField.createObject(medGrid,{});
                scheduleField.createObject(medGrid,{});
                deleteButton.createObject(medGrid,{});
            }

            function killTheZombie() {
                for(var i=5;i<medGrid.children.length;i+=5) {
                    var killBut = medGrid.children[i+4];
                    if(killBut.text.length > 6) {
                        var kill1 = medGrid.children[i];
                        var kill2 = medGrid.children[i+1];
                        var kill3 = medGrid.children[i+2];
                        var kill4 = medGrid.children[i+3];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        kill4.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }

        Button {
            id: addButton
            //icon.name: "list-add"
            text: "Add another drug"
            opacity: 0
            onClicked: medGrid.makeNewRow();
            Material.accent: Material.Green
            highlighted: true

        }

        Button {
            id: doneButton
            text: "Done"
            Layout.alignment: Qt.AlignHCenter
            opacity: 0
            onClicked: {
                var medList = [];
                for(var i=5;i<medGrid.children.length;i+=5) {
                    var med = ({});
                    var drugNameBox = medGrid.children[i];
                    var usedFor = medGrid.children[i+1];
                    var amount = medGrid.children[i+2];
                    var schedule = medGrid.children[i+3];

                    med["drugName"] = drugNameBox.text;
                    med["usedFor"] = usedFor.text;
                    med["amount"] = amount.text;
                    med["schedule"] = schedule.currentText;
                    medList.push(med);
                }
                saveProps["medList"] = medList;
                stackView.push("Allergies.qml");
            }
        }


    }

    SequentialAnimation {
        id: showtheDrugs
        PropertyAnimation {
            target: t1
            from: 1
            to: 0
            property: "opacity"
            duration: 300
        }

        PropertyAnimation {
            target: yesNowCol
            from: 1
            to: 0
            property: "opacity"
            duration: 300
        }
        ScriptAction {
            script:  {
                yesNowCol.visible = false;
                medGrid.makeNewRow();
            }
        }
        PropertyAnimation {
            target: medGrid
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PropertyAnimation {
            target: addButton
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }

        PropertyAnimation {
            target: doneButton
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: yesNowCol
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
