import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Page {
    property var systemName
    property var conditionList

    signal weAreDoneHere

    title: "Other conditions"

    Label {
        id: name
        text: "Are there any other conditions not previously listed?";
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        font.pointSize: 24
        wrapMode: Text.WordWrap
        width: rootWin.width * .8
    }

    ColumnLayout {
        id: yesNoRow
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes, there is something\nI haven't told you yet"
            Material.accent: Material.Red
            highlighted: true
            Layout.alignment: Qt.AlignHCenter
            onClicked:{
                yesNoRow.opacity=0;
                name.opacity=0;
                moreCondColumn.visible = true;
                moreCondColumn.opacity=1;
            }

        }
        Button {
            id: noButton
            text: "No, there is no other\nmedical condition I know about"
            Material.accent: Material.Blue
            highlighted: true
            onClicked: weAreDoneHere();
            Layout.alignment: Qt.AlignHCenter
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    Column {
        id: moreCondColumn
        visible: false
        opacity: 0
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Label {
            id: pleaseMentionText
            text: "Please mention all of them here:" +
                  "\n(please seperate each one with a new line)"
            font.bold: true
        }

        Rectangle {
            width: rootWin.width / 2;
            height: rootWin.height - 350
            border.color: Material.color(Material.Grey)
            border.width: 2

            TextArea {
                id: otherConditionBox
                anchors.fill: parent
                wrapMode: TextEdit.WordWrap
            }
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }


    footer: Button {
        id: doneWithConButton
        text: "Done"
        opacity: moreCondColumn.visible ? 1:0
        Material.accent: Material.Green
        anchors.horizontalCenter: parent.horizontalCenter
        highlighted: true
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked: {
            var otherCondNames = otherConditionBox.text.split("\n");
            for(var i=0;i<otherCondNames.length;i++) {
                mainSwipeView.medicalConditionList[otherCondNames[i]] =
                        otherCondNames[i];
            }
            weAreDoneHere();
        }
    }

}
