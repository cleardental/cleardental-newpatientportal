import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Lets talk about your pain..."
    contentWidth: painCol.implicitWidth
    contentHeight: painCol.implicitHeight
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: painCol
        x: painCol.implicitWidth > rootWin.width ?
               0 : (rootWin.width - painCol.implicitWidth) /2

        Label {
            id: t1
            text: "Sorry to hear about your pain."
            font.pointSize: 24
            opacity: 0
        }
        Label {
            id: t2
            text: "In order to best treat you,\nwe need to ask you additional"+
                  " information."
            opacity: 0
        }
        ColumnLayout {
            id: theRest
            opacity: 0
            MenuSeparator {}
            Label {
                text: "How long ago did you start getting pain?"
                font.bold: true
            }
            RadioButton {
                text: "Recently (less than 24 hours ago)"
                id: lessThan1Day
            }
            RadioButton {
                id: someTimeAgo
                text: "Some time ago (between a day and 2 weeks ago)"
            }
            RadioButton {
                id: forSomeWhile
                text: "I've had it for a while now (2 weeks to 6 months)"
            }
            RadioButton {
                id: longTime
                text: "For a long time (more than 6 months)"
            }
            ButtonGroup {
                id: painLength
                buttons: [lessThan1Day,someTimeAgo,forSomeWhile,longTime]
            }

            MenuSeparator {}
            Label {
                text: "Where do you feel it (the most)?"
                font.bold: true
            }
            RadioButton {
                id: ur
                text: "Upper Back Right"
            }
            RadioButton {
                id: uc
                text: "Upper Front"
            }
            RadioButton {
                id: ul
                text: "Upper Back Left"
            }
            RadioButton {
                id: lr
                text: "Lower Back Right"
            }
            RadioButton {
                id: lc
                text: "Lower Font"
            }
            RadioButton {
                id: ll
                text: "Lower Back Left"
            }
            ButtonGroup {
                id: painLocation
                buttons: [ur,ul,lr,ll,uc,lc]
            }

            MenuSeparator{}

            Label {
                text: "Assuming that the tooth can be saved,\n" +
                      "would you be interested in saving the tooth?"
                font.bold: true
            }

            RadioButton {
                id: saveIt
                text: "Yes, I wish to save the tooth"
            }

            RadioButton {
                id: leaveIt
                text: "No, I have no interest in saving the tooth"
            }

            RadioButton {
                id: depends
                text: "Depends on how much it would cost me"
            }

            RowLayout {
                visible: opacity != 0
                opacity: depends.checked ? 1 : 0
                Label {
                    text: "What is your budget?"
                }
                TextField {
                    id: patBudget
                }

                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }
            }

            ButtonGroup {
                id: saveTooth
                buttons: [saveIt,leaveIt,depends]
            }


            MenuSeparator{}

            Label {
                text: "How soon can you come?"
                font.bold: true
            }
            RadioButton {
                id: comeToday
                text: "I am come today"
            }
            RadioButton {
                id: comeLater
                text: "I can't make it today, but I want to be seen ASAP"
            }
            ButtonGroup {
                id: whenCome
                buttons: [comeLater,comeToday]
            }

            MenuSeparator{}

            Button {
                text: "Next"
                opacity: (painLength.checkedButton != null) &&
                         (painLocation.checkedButton != null) &&
                         (whenCome.checkedButton != null) ? 1 :0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 300
                    }
                }

                onClicked: {
                    saveProps["painLength"] = painLength.checkedButton.text;
                    saveProps["painLocation"] = painLocation.checkedButton.text;
                    saveProps["saveTooth"] = saveTooth.checkedButton.text;
                    if(depends.checked) {
                        saveProps["saveToothBudget"] = patBudget.text;
                    }
                    saveProps["whenCome"] = whenCome.checkedButton.text;
                    stackView.push("BasicPersonalInfo.qml");
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: t2
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
