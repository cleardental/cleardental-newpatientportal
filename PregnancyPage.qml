import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Page {
    property var systemName
    property var conditionList

    signal weAreDoneHere

    title: "For women:"

    ColumnLayout {
        id: breastFeedingColumn
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10
        Label {
            text: "Are you currently breast feeding?"
            verticalAlignment: Text.AlignVCenter
            height: obgynName.height
            font.bold: true
        }
        RowLayout {
            Label {
                Layout.fillWidth: true
            }

            RadioButton {
                id: yesBreastFeeding
                text: "Yes"
                onCheckedChanged: {
                    rootWin.saveProps["breastFeeding"] =
                            yesBreastFeeding.checked;
                }
            }
            RadioButton {
                text: "No"
                checked: true
            }
            Label {
                Layout.fillWidth: true
            }
        }
    }

    Text {
        id: name
        text: "Are you currently or trying to become pregnant?";
        font.bold: true
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: breastFeedingColumn.bottom
        anchors.topMargin: 10
    }

    RowLayout {
        id: yesNoRow
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes"
            onClicked:{
                yesNoRow.opacity=0;
                moreInfoGrid.visible = true;
                moreInfoGrid.opacity = 1;
            }
            Material.accent: Material.Red
            highlighted: true
        }
        Button {
            id: noButton
            text: "No"
            onClicked: weAreDoneHere();
            Material.accent: Material.Blue
            highlighted: true
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    GridLayout {
        id: moreInfoGrid
        columns: 2
        anchors.top: name.bottom
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0
        visible: false

        Label {
            text: "What is the due date?"
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            height: dueDate.height
        }

        DatePicker {
            id: dueDate
            Component.onCompleted: {
                setDate((new Date()).toLocaleString(Qt.locale(),"MM/d/yyyy"))
            }
        }

        Label {
            text: "What is the name of the OB/GYN?"
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            height: obgynName.height
        }

        TextField {
            id: obgynName
            placeholderText: "Name of the OB/GYN"
            Layout.fillWidth: true
        }



        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    footer: Button {
        id: doneWithConButton
        text: "Done"
        Material.accent: Material.Green
        anchors.horizontalCenter: parent.horizontalCenter
        highlighted: true
        opacity: moreInfoGrid.opacity==1 ? 1:0
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked:{
            weAreDoneHere();
            rootWin.saveProps["pregnant"] = true;
            rootWin.saveProps["pregnantDueDate"] = dueDate.getDate();
            rootWin.saveProps["pregnantOBGYN"] = obgynName.text;
        }
    }

}
