import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Page {
    property var systemName
    property var conditionList

    signal weAreDoneHere

    title: "Premed"

    Label {
        id: name
        text: "Has anyone ever told you to take a antiboitic premedication " +
              "before dental treatment?";
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        font.pointSize: 24
        width: rootWin.width * .8
        wrapMode: Text.WordWrap
    }

    ColumnLayout {
        id: yesNoRow
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: name.height
        spacing: yesButton.height
        Button {
            id: yesButton
            text: "Yes, somebody has"
            Material.accent: Material.Red
            highlighted: true
            Layout.alignment: Qt.AlignHCenter
            onClicked:{
                yesNoRow.opacity=0;
                name.opacity=0;
                moreCondColumn.visible = true;
                moreCondColumn.opacity=1;
            }

        }
        Button {
            id: noButton
            text: "No, nobody has ever told me to do so"
            Material.accent: Material.Blue
            highlighted: true
            onClicked: {
                rootWin.saveProps["needPreMed"] = false;
                weAreDoneHere();
            }
            Layout.alignment: Qt.AlignHCenter
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }

    GridLayout {
        id: moreCondColumn
        visible: false
        opacity: 0
        columns: 2
        anchors.top: name.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            font.bold: true
            text: "Name of doctor who told you"
        }

        TextField {
            id: whoTold
        }

        Label {
            font.bold: true
            text: "What do you normally take for premedication?"
        }


        TextField {
            id: whatTake
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }
    }


    footer: Button {
        id: doneWithConButton
        text: "Done"
        opacity: moreCondColumn.visible ? 1:0
        Material.accent: Material.Green
        anchors.horizontalCenter: parent.horizontalCenter
        highlighted: true
        Behavior on opacity {
            NumberAnimation {
                duration: 500
            }
        }

        onClicked: {
            rootWin.saveProps["needPreMed"] = true;
            rootWin.saveProps["whoToldTakePremed"] =whoTold.text
            rootWin.saveProps["whatTakePremed"] =whatTake.text
            weAreDoneHere();
        }
    }

}
