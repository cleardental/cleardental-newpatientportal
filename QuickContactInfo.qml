import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    property string title: "Contact Info"
    contentWidth: addContactInfo.implicitWidth
    contentHeight: addContactInfo.implicitHeight
    ScrollBar.vertical: ScrollBar { }
    ColumnLayout {
        id: addContactInfo
        x: addContactInfo.implicitWidth > rootWin.width ?
               0 : (rootWin.width - addContactInfo.implicitWidth) /2
        Label {
            id: t1
            text: "What is the best way to contact you?"
            font.pointSize: 24
            opacity: 0
            Layout.maximumWidth: rootWin.width
            wrapMode: Text.WordWrap
        }
        GridLayout {
            id: theRest
            opacity: 0
            columns: 2

            ComboBox {
                id: bestMethodBox
                model: ["Call my cell", "Text me", "Call home","Email me",
                    "Call work","WhatsApp", "Facebook message me" ]
                Layout.fillWidth: true
                Layout.columnSpan: 2
                onCurrentIndexChanged: methodLabel.updateLabel();
            }
            Label {
                id: methodLabel

                function updateLabel() {
                    switch(bestMethodBox.currentIndex) {
                    case 0:
                    case 1:
                        text = "Cell phone Number"
                        break;
                    case 2:
                        text = "Home phone Number"
                        break;
                    case 3:
                        text = "Email address"
                        break;
                    case 4:
                        text = "Work address"
                        break;
                    case 5:
                        text = "WhatsApp number"
                        break;
                    case 6:
                        text = "Facebook Name/URL"
                        break;
                    }
                }

            }
            TextField {
                id: contactField
                Layout.fillWidth: true
            }

            Button {
                text: "Next"
                opacity: contactField.text.length > 2 ? 1 : 0
                Behavior on opacity {
                    PropertyAnimation {
                        duration: 1000
                    }
                }
                onClicked: {
                    saveProps["howToContact"] = bestMethodBox.currentText;
                    saveProps["howToContactAddr"] = contactField.text
                    stackView.push("DentalPlanInfo.qml");
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: theRest
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
