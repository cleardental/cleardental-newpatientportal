import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ScrollView {
    property string title: "Secondary Dental Plan"
    ColumnLayout {
        anchors.centerIn: parent

        Label {
            id: t1
            text: "Secondary Dental Plan info"
            font.pointSize: 24
            opacity: 0
            font.underline: true
        }

        GridLayout {
            id: secondIns
            opacity: 0
            columns: 2
            Label {
                text: "Insured’s Name"
                font.bold: true
            }
            TextField {
                id: secInsName
                Layout.fillWidth: true
            }
            Label {
                text: "Relationship to Patient"
                font.bold: true
            }
            ComboBox {
                id: secInsRelToPat
                model: ["Self", "Parent", "Spouse"]
                editable: true
                Layout.fillWidth: true
            }
            Label {
                text: "Social Security #"
                font.bold: true
            }
            TextField {
                id: secInsSSN
                Layout.fillWidth: true
            }
            Label {
                text: "Insurance Company"
                font.bold: true
            }
            TextField {
                id: secInsCompany
                Layout.fillWidth: true
            }
            Label {
                text: "Group Number"
                font.bold: true
            }
            TextField {
                id: secInsGroup
                Layout.fillWidth: true
            }
            Label {
                text: "Subscriber ID"
                font.bold: true
            }
            TextField {
                id: secInsSubID
                Layout.fillWidth: true
            }
            Label {
                text: "Insurance Company Address"
                font.bold: true
            }
            TextField {
                id: secInsAddr
                Layout.fillWidth: true
            }
            MenuSeparator {Layout.columnSpan:2}
            Behavior on opacity {
                PropertyAnimation {
                    duration: 500
                }
            }
        }

        Button {
            text: "Next"
            onClicked: {
                saveProps["secInsName"] = secInsName.text;
                saveProps["secInsRelToPat"] = secInsRelToPat.currentText;
                saveProps["secInsSSN"] = secInsSSN.text;
                saveProps["secInsCompany"] = secInsCompany.text;
                saveProps["secInsGroup"] = secInsGroup.text;
                saveProps["secInsSubID"] = secInsSubID.text;
                saveProps["secInsAddr"] = secInsAddr.text;
                if(saveProps["pain"]) {
                    stackView.push("MedicalHistory.qml");
                }
                else {
                    stackView.push("DentalHistory.qml");
                }
            }
        }
    }

    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: secondIns
            from: 0
            to: 1
            property: "opacity"
            duration: 500
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }
}
