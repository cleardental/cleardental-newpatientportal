import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Flickable {
    id: startView
    property string title: "Welcome!"
    contentWidth: startPageCol.implicitWidth
    contentHeight: startPageCol.implicitHeight
    ScrollBar.vertical: ScrollBar { }

    ColumnLayout {
        id: startPageCol

        x: startPageCol.implicitWidth >
                            rootWin.width ?
                                0 :
                                (rootWin.width -
                                 startPageCol.implicitWidth) /2

        Image {
            Layout.maximumWidth: 256
            Layout.maximumHeight: 256
            source: "qrc:/icons/zenLogo.png"
            Layout.alignment: Qt.AlignHCenter
            smooth: true

        }



        Label {
            id: t1
            text: "Welcome to Zen Family Dental!"
            font.pointSize: 24
            opacity: 0
            Layout.maximumWidth: rootWin.width
            wrapMode: Text.WordWrap
        }
        Label {
            id: t2
            text: "Before we get started, we need to ask you a few questions."
            opacity: 0
            wrapMode: Text.WordWrap
        }
        Label {
            id: t3
            text: "Are you in any pain?"
            opacity: 0
        }
        Button {
            id: b1
            text: "Yes, I am in pain"
            opacity: 0
            Material.accent: Material.Red
            highlighted: true
            onClicked: {
                saveProps["pain"] = true;
                stackView.push("PainPage.qml");
            }
        }
        Button {
            id: b2
            text: "No, I am not in any pain right now"
            opacity: 0
            Material.accent: Material.Blue
            highlighted: true
            onClicked: {
                saveProps["pain"] = false;
                stackView.push("ChiefComplaint.qml");
            }
        }
    }



    SequentialAnimation {
        id: startWelcome
        PropertyAnimation {
            target: t1
            from: 0
            to: 1
            property: "opacity"
            duration: 1000
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: t2
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: t3
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PauseAnimation {
            duration: 500
        }
        PropertyAnimation {
            target: b1
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
        PropertyAnimation {
            target: b2
            from: 0
            to: 1
            property: "opacity"
            duration: 300
        }
    }

    Component.onCompleted: {
        startWelcome.start();
    }


}
