import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ColumnLayout {
    id: reviewColumn
    property var systemName
    property var conditionList

    signal weAreDoneHere

    Label {
        id: name
        Layout.alignment: Qt.AlignHCenter
        text: "Do you have any <b>"  + systemName + "</b> that you know of?";
        wrapMode: Text.WordWrap
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: exampleTexts
        Layout.alignment: Qt.AlignHCenter
        Layout.maximumWidth: rootWin.width /2
        wrapMode: Text.Wrap
        color: Material.color(Material.Grey)
        font.pointSize: 8

        text:  {
            var returnMe="This includes: ";
            for(var cond in conditionList) {
                returnMe += conditionList[cond] + ", ";
            }
            returnMe = returnMe.substring(0, returnMe.length - 2);
            returnMe += ".";

            return returnMe;
        }
    }

    ColumnLayout {
        id: yesNoRow
        spacing: yesButton.height
        Layout.alignment: Qt.AlignHCenter

        Button {
            id: yesButton
            text: "Yes, I may have\n" + systemName
            Material.accent: Material.Red
            highlighted: true
            Layout.alignment: Qt.AlignHCenter
            onClicked:{
                showTheConditionsAnimation.start();
            }
        }
        Button {
            id: noButton
            text: "No, I don't have any of those conditions"
            Material.accent: Material.Blue
            highlighted: true
            onClicked: weAreDoneHere();
        }
    }

    SequentialAnimation {
        id: showTheConditionsAnimation
        PropertyAnimation {
            targets: [name,exampleTexts,yesNoRow]
            property: "opacity"
            from: 1
            to: 0
            duration: 300
        }
        ScriptAction {
            script: {
                name.visible = false;
                exampleTexts.visible = false;
                yesNoRow.visible = false;
                conditionListView.visible = true;
                conditionModel.makeConditionModel();
            }
        }
    }

    Timer {
        id: addTimer
        interval: 100
        repeat: true

        onTriggered: {
            if(!conditionModel.done) {
                conditionModel.addNextItem();
            } else {
                repeat = false;
            }
        }
    }

    ListModel {
        id: conditionModel

        property bool done: false
        property int currIndex: 0

        function makeConditionModel() {
            done = false;
            currIndex =0;
            addNextItem();
            addTimer.start();
        }

        function addNextItem() {
            conditionModel.append({conditionName:conditionList[currIndex]});
            currIndex++;
            if(currIndex >= conditionList.length) {
                done = true;
            }
        }
    }

    Component {
        id: contitionComp
        Rectangle {
            id: rectComp
            height: conText.checked ?
                        conText.height + explainMoreField.height+ 10 :
                        conText.height + 5
            radius: 5
            width: conditionListView.width
            border.width: 1
            border.color: "black"
            color: Material.color(Material.LightBlue, Material.Shade50)
            ColumnLayout {
                CheckBox {
                    id: conText
                    Layout.leftMargin: 5
                    text: conditionName
                    font.pointSize: 12
                    width: parent.width
                    onCheckStateChanged: {
                        if(conText.checked) {
                            mainSwipeView.medicalConditionList[conditionName] =
                                    "true";
                        } else {
                            //too complicated to remove the key
                            mainSwipeView.medicalConditionList[conditionName] =
                                    "false";
                        }
                    }

                }
                TextField {
                    id: explainMoreField
                    Layout.leftMargin: 10
                    opacity: conText.checked ? 1 : 0
                    implicitWidth: rectComp.width - 20
                    placeholderText: "Please explain more about the " +
                                     conditionName
                    Behavior on opacity {
                        NumberAnimation {
                            duration: 300
                        }
                    }

                    onTextChanged: {
                        mainSwipeView.medicalConditionList[
                                    conditionName]=explainMoreField.text
                    }
                }
            }

            Behavior on height {
                NumberAnimation {
                    duration: 250
                }
            }
        }
    }

    ListView {
        id: conditionListView
        model: conditionModel
        clip: true
        delegate: contitionComp
        spacing: 5
        ScrollBar.vertical: ScrollBar { }
        visible: false
        Layout.minimumWidth: rootWin.width * 0.8
        Layout.minimumHeight: rootWin.height  - t1.height - 100
        Layout.maximumHeight: rootWin.height  - t1.height - 100

        add: Transition {
            NumberAnimation { property: "scale";
                from: 0; to: 1.0; duration: 500 }
            NumberAnimation { properties: "opacity";
                from: 0; to: 1; duration: 500 }
        }

        footerPositioning: ListView.OverlayFooter

        footer: Button {
            id: doneWithConButton
            text: "Done with condition"
            anchors.horizontalCenter: parent.horizontalCenter
            Material.accent: Material.Green
            highlighted: true
            opacity: conditionListView.visible ? 1:0
            Behavior on opacity {
                NumberAnimation {
                    duration: 500
                }
            }
            z: 1024
            onClicked: weAreDoneHere();
        }
    }

}
