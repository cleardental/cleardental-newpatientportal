import QtQuick 2.9
import QtQuick.Controls 2.2

ApplicationWindow {
    id: rootWin
    visible: true
    width: minAppWidth
    height: minAppHeight
    title: qsTr("Welcome to Zen Family Dental!")
    property var saveProps: ({})
    property int minAppWidth: 640
    property int minAppHeight: 640


//    FontLoader {
//        id: barlow
//        source: "https://clear.dental/fonts/Barlow_Semi_Condensed/BarlowSemiCondensed-Regular.ttf"

//    }

    font.pointSize: 12
    font.family: "Barlow Semi Condensed"


    header: ToolBar {
        ToolButton {
            id: toolButton
            icon.source: "qrc:///icons/go-previous.png"
            visible: stackView.depth > 1
            onClicked: {
                if(stackView.currentItem.title ==="Medical history") {
                    stackView.currentItem.goBackCondition()
                }
                else {
                    stackView.pop();
                }
            }
        }
        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
            font.pointSize: 16
        }
    }

    StackView {
        id: stackView
        initialItem: "StartPage.qml"
        anchors.fill: parent
    }


}
